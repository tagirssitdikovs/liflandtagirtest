#имя базового образа
FROM golang:latest

#создаем папку, где будет наша программа
RUN mkdir -p /go/src/liflandtest

#идем в папку
WORKDIR /go/src/liflandtest

COPY . /go/src/liflandtest
RUN ls -la /go/src/liflandtest/*
RUN ls -la /go/src/liflandtest/dbcode/*

#скачиваем зависимые пакеты через скрипт, любезно разработанный командой docker
RUN go-wrapper download

#инсталлируем все пакеты и вашу программу
RUN go-wrapper install

#запускаем вашу программу через тот же скрипт, чтобы не зависеть от ее скомпилированного имени
#-web - это параметр, передаваемый вашей программе при запуске, таких параметров может быть сколько угодно
CMD ["go-wrapper", "run", "-web"]

#пробрасываем порт вашей программы наружу образа
EXPOSE 8080
#FROM golang:onbuild
#EXPOSE 8080
#
# Имя liflandtesttagir/liflandtestreposirory взять из https://hub.docker.com,
# то есть должно совпадать, а иначе push не будет работать
#
# <Run as administrator>
# cd C:\Users\User\go\src\liflandtest
#
# docker images
# docker rmi liflandtesttagir/liflandtestreposirory
# docker build -t liflandtesttagir/liflandtestreposirory .
#
# docker run --publish 6060:8080 --name test --rm liflandtesttagir/liflandtestreposirory
#
# http://localhost:6060/reset
# http://localhost:6060/fund?playerId=P1&points=300
# http://localhost:6060/balance?playerId=P1
#
# docker login
# Docker id: liflandtesttagir
# Passw: Z156aurDFr
# Login Succeeded
#
# docker push liflandtesttagir/liflandtestreposirory
# docker pull liflandtesttagir/liflandtestreposirory
#
# git clone https://bitbucket.org/tagirssitdikovs/liflandtagirtest
#
