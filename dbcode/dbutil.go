package dbcode

// https://github.com/mattn/go-sqlite3/blob/master/_example/simple/simple.go
//

import (
	"fmt"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"os"
)

func Dbinittest() int {
	os.Remove("./lifland.db")

	db, err := sql.Open("sqlite3", "./lifland.db")
	if err != nil {
		return 400
	}
	defer db.Close()

	sqlStmt := "drop table player"
	_, _ = db.Exec(sqlStmt)

	sqlStmt = "drop table tournament"
	_, _ = db.Exec(sqlStmt)

	sqlStmt = "drop table tourplayer"
	_, _ = db.Exec(sqlStmt)

	sqlStmt = `
	CREATE TABLE player (
    	playerid    varchar(5) not null primary key,
    	balance     integer NOT NULL
	)`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return 400
	}

	sqlStmt = `
	CREATE TABLE tournament (
    	tourid    integer primary key autoincrement not null,
    	deposit     integer NOT NULL
	)`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return 400
	}

	sqlStmt = `
	CREATE TABLE tourplayer (
		tourplayid  integer not null primary key,
    	tourid    integer,
		mainplayerid     varchar(5) NULL,
		playerid     varchar(5) NULL,
		contribute  integer default 0,
		balancenew  integer default 0
	)`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return 400
	}
	return 200
}

func Dbfund(playerId string, points int) int {
	db, err := sql.Open("sqlite3", "./lifland.db")
	if err != nil {
		return 400
	}
	defer db.Close()

	stmt, err := db.Prepare("select count(*) reccount from player where playerId=?")
	if err != nil {
		return 400
	}

	var reccount int
	err = stmt.QueryRow(playerId).Scan(&reccount)
	if err != nil {
		return 400
	}

	if reccount == 0 {
		stmt, err := db.Prepare("insert into player(playerId,balance) values(?, ?)")
		if err != nil {
			return 400
		}
		_, err = stmt.Exec(playerId, points)
		if err != nil {
			return 400
		}

	} else {
		stmt, err := db.Prepare("update player set balance = balance+? where playerId=?")
		if err != nil {
			return 400
		}
		_, err = stmt.Exec( points, playerId)
		if err != nil {
			return 400
		}

	}
	return 200
}

func DbannounceTournament(tournamentId int, deposit int) int {
	db, err := sql.Open("sqlite3", "./lifland.db")
	if err != nil {
		return 400
	}
	defer db.Close()

	stmt, err := db.Prepare("select count(*) reccount from tournament where tourid=?")
	if err != nil {
		return 400
	}
	var reccount int
	err = stmt.QueryRow(tournamentId).Scan(&reccount)
	if err != nil {
		return 400
	}

	if reccount == 0 {
		stmt, err := db.Prepare("insert into tournament(tourid, deposit) values(?, ?)")
		if err != nil {
			return 400
		}
		_, err = stmt.Exec(tournamentId, deposit)
		if err != nil {
			return 400
		}
	} else {
		stmt, err := db.Prepare("update tournament set deposit = ? where tourid=?")
		if err != nil {
			return 400
		}
		_, err = stmt.Exec(deposit, tournamentId)
		if err != nil {
			return 400
		}
	}
	return 200
}

func DbjoinTournament(tournamentId int, playerId string, backerIds [] string) int {
	db, err := sql.Open("sqlite3", "./lifland.db")
	if err != nil {
		return 400
	}
	defer db.Close()

	stmt, err := db.Prepare("select deposit from tournament where tourid=?")
	if err != nil {
		return 400
	}
	var deposit int
	err = stmt.QueryRow(tournamentId).Scan(&deposit)
	if err != nil {
		return 400
	}

	var playercount int
	playercount = 1
	if len(backerIds) >= 1 {
		playercount += len(backerIds)
	}
	var contribute int
	contribute = deposit / playercount

	stmt, err = db.Prepare("insert into tourplayer(tourid, mainplayerid, playerid, contribute) values(?, ?, ?, ?)")
	if err != nil {
		return 400
	}
	_, err = stmt.Exec(tournamentId, playerId, playerId, contribute)
	if err != nil {
		return 400
	}

	if len(backerIds) >= 1 {
		stmt, err = db.Prepare("insert into tourplayer(tourid, mainplayerid, playerid, contribute) values(?, ?, ?, ?)")
		if err != nil {
			return 400
		}

		for _, v := range backerIds {
			_, err = stmt.Exec(tournamentId, playerId, v, contribute)
			if err != nil {
				return 400
			}
		}
	}
	return 200
}

func Dbwinner(mainplayerid string, mainprize int) int {
	statuscode := Dbwinner1(mainplayerid, mainprize)
	if statuscode != 200{
		return statuscode
	}
	statuscode = Dbwinner2()
	if statuscode != 200{
		return statuscode
	}
	return 200
}

func Dbwinner1(mainplayerid string, mainprize int) int {
	db, err := sql.Open("sqlite3", "./lifland.db")
	if err != nil {
		return 400
	}
	defer db.Close()
	db.Exec("PRAGMA journal_mode = wal")

	stmt, err := db.Prepare("select count(*) playercount from tourplayer where mainplayerid=?")
	if err != nil {
		return 400
	}
	var playercount int
	err = stmt.QueryRow(mainplayerid).Scan(&playercount)
	if err != nil {
		return 400
	}

	stmt, err = db.Prepare("select tourplayid, playerid, contribute from tourplayer where mainplayerid=?")
	rows, err := stmt.Query(mainplayerid)
	if err != nil {
		return 400
	}
	defer rows.Close()
	var playerid string
	var contribute int
	var balance int
	var tourplayid int
	var balancenew int
	for rows.Next() {
		err := rows.Scan(&tourplayid, &playerid, &contribute)
		if err != nil {
			return 400
		}

		stmt, err := db.Prepare("select balance from player where playerid=?")
		if err != nil {
			return 400
		}
		err = stmt.QueryRow(playerid).Scan(&balance)
		if err != nil {
			return 400
		}

		balancenew = balance - contribute + (mainprize / playercount)

		stmt, err = db.Prepare("update tourplayer set balancenew=? where tourplayid=?")
		if err != nil {
			return 400
		}
		_, err = stmt.Exec(balancenew, tourplayid)
		if err != nil {
			return 400
		}
	}
	err = rows.Err()
	if err != nil {
		return 400
	}

	return 200
}

func Dbwinner2() int {
	db, err := sql.Open("sqlite3", "./lifland.db")
	if err != nil {
		return 400
	}
	defer db.Close()
	db.Exec("PRAGMA journal_mode = wal")

	var playerid string
	var balancenew int

	stmt, err := db.Prepare("select playerid, balancenew from tourplayer")
	rows, err := stmt.Query()
	if err != nil {
		return 400
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&playerid, &balancenew)
		if err != nil {
			return 400
		}

		stmt, err = db.Prepare("update player set balance=? where playerid=?")
		if err != nil {
			return 400
		}
		_, err = stmt.Exec(balancenew, playerid)
		if err != nil {
			return 400
		}
	}

	return 200
}

func Dbbalance(playerId string) (int, string) {
	db, err := sql.Open("sqlite3", "./lifland.db")
	if err != nil {
		return 400,""
	}
	defer db.Close()

	stmt, err := db.Prepare("select balance from player where playerid=?")
	if err != nil {
		return 400,""
	}
	var balance int
	err = stmt.QueryRow(playerId).Scan(&balance)
	if err != nil {
		return 400,""
	}

	result := fmt.Sprintf("%s%s%s%d%s", "{\"playerId\": \"", playerId, "\", \"balance\":", balance, "}")
	return 200,result
}
