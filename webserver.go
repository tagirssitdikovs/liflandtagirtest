package main

//
// LiflandGaming test
// Tagirs Sitdikovs
// https://golang.org/src/net/http/status.go
//

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"sync"
	"encoding/json"
	"io/ioutil"
	"strings"
	D "liflandtest/dbcode"
)

type winnerpost struct {
	Winners []struct {
		PlayerID string
		Prize    int
	}
}

var counter int
var mutex = &sync.Mutex{}

func reset(w http.ResponseWriter, r *http.Request) {
	statuscode := D.Dbinittest()
	if statuscode != 200 {
		http.Error(w, "Custom error "+strconv.Itoa(statuscode), statuscode)
	}
}

func take(w http.ResponseWriter, r *http.Request) {
	mutex.Lock()
	playerId := r.FormValue("playerId")
	pointsString := r.FormValue("points")
	points, err := strconv.Atoi(pointsString)
	if err != nil {
		panic(err)
	}
	statuscode := D.Dbfund(playerId, -points)
	if statuscode != 200 {
		http.Error(w, "Custom error "+strconv.Itoa(statuscode), statuscode)
	}
	mutex.Unlock()
}

func fund(w http.ResponseWriter, r *http.Request) {
	mutex.Lock()
	playerId := r.FormValue("playerId")
	pointsString := r.FormValue("points")
	points, err := strconv.Atoi(pointsString)
	if err != nil {
		panic(err)
	}
	statuscode := D.Dbfund(playerId, points)
	if statuscode != 200 {
		http.Error(w, "Custom error "+strconv.Itoa(statuscode), statuscode)
	}
	mutex.Unlock()
}

func announceTournament(w http.ResponseWriter, r *http.Request) {
	mutex.Lock()
	tournamentIdString := r.FormValue("tournamentId")
	depositString := r.FormValue("deposit")
	tournamentId, err := strconv.Atoi(tournamentIdString)
	if err != nil {
		panic(err)
	}
	deposit, err := strconv.Atoi(depositString)
	if err != nil {
		panic(err)
	}
	statuscode := D.DbannounceTournament(tournamentId, deposit)
	if statuscode != 200 {
		http.Error(w, "Custom error "+strconv.Itoa(statuscode), statuscode)
	}
	mutex.Unlock()
}

func joinTournament(w http.ResponseWriter, r *http.Request) {
	mutex.Lock()
	backerIds, _ := r.URL.Query()["backerId"]
	tournamentIdString := r.FormValue("tournamentId")
	playerId := r.FormValue("playerId")
	tournamentId, err := strconv.Atoi(tournamentIdString)
	if err != nil {
		panic(err)
	}
	statuscode := D.DbjoinTournament(tournamentId, playerId, backerIds)
	if statuscode != 200 {
		http.Error(w, "Custom error "+strconv.Itoa(statuscode), statuscode)
	}
	mutex.Unlock()
}

func winner(w http.ResponseWriter, r *http.Request) {
	//log.Println(r.Method)
	body, err := ioutil.ReadAll(r.Body)
	bodyString := string(body)
	bodyString = strings.Replace(bodyString, "\\", "", -1)
	//log.Println(bodyString)
	mutex.Lock()
	decoder := json.NewDecoder(strings.NewReader(bodyString))
	var t winnerpost
	err = decoder.Decode(&t)
	if err != nil {
		body, err := ioutil.ReadAll(r.Body)
		bodyString = string(body)
		//log.Println(bodyString)
		panic(err)
	}
	defer r.Body.Close()

	playerId := t.Winners[0].PlayerID
	prize := t.Winners[0].Prize
	statuscode := D.Dbwinner(playerId, prize)
	if statuscode != 200 {
		http.Error(w, "Custom error "+strconv.Itoa(statuscode), statuscode)
	}
	mutex.Unlock()
}

func balance(w http.ResponseWriter, r *http.Request) {
	mutex.Lock()
	playerId := r.FormValue("playerId")
	statuscode, result := D.Dbbalance(playerId)
	if statuscode != 200 {
		http.Error(w, "Custom error "+strconv.Itoa(statuscode), statuscode)
	}
	fmt.Fprintf(w, result)
	mutex.Unlock()
}

func main() {

	http.HandleFunc("/reset", reset)

	http.HandleFunc("/take", take)

	http.HandleFunc("/fund", fund)

	http.HandleFunc("/announceTournament", announceTournament)

	http.HandleFunc("/joinTournament", joinTournament)

	http.HandleFunc("/resultTournament", winner)

	http.HandleFunc("/balance", balance)

	log.Fatal(http.ListenAndServe(":8080", nil))
/*

http://localhost:8080/reset

*/
}
